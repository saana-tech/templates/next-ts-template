module.exports = new Proxy(
  {},
  {
    get: function getter() {
      return () => ({
        className: "className",
        variable: "inter",
        style: { fontFamily: "inter" },
      });
    },
  },
);
