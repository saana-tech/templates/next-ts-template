import type { FC, PropsWithChildren } from "react";

import type { Metadata } from "next";
import { Inter } from "next/font/google";

import "./globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Next Template",
  description: "Generated by Next Template",
};

interface IRootLayout extends PropsWithChildren {}

const RootLayout: FC<IRootLayout> = ({ children }) => (
  <html lang="en">
    <body className={inter.className}>{children}</body>
  </html>
);

export default RootLayout;
